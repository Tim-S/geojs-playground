import * as proj4 from 'proj4/dist/proj4.js';
import * as geo from 'geojs/geo.min.js';

export function ppi(map, center, options={}){
    let defaults = {
        distances : [1000, 5000, 10000, 20000, 30000],
        stroke:"black",
        strokeWidth: "3px"
    };
    let config = Object.assign({}, defaults, options);

    createDistanceIndicator(map, center, config.distances, config);
}

function createDistanceIndicator(map, center, distances, options){
    //Create layer for distance-rings 
    var layer = map.createLayer('feature', { renderer:'d3' });
    var svg = layer.canvas();
    var g = svg.append('g').classed('layer', true);
    
    // Azimuthal Equidistant projection
    var aeqd = `+proj=aeqd +lat_0=${center.y} +lon_0=${center.x} +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs`;

    //for every distance create a milestone in north direction
    var milestones = distances
                            .map( distance => [0, distance] )
                            .map( en => proj4(aeqd).inverse(en) )
                            .map( gps => { return {x:gps[0], y:gps[1]}; })
    var displayDistances = milestones.map( milestone => displayDistance(map, center, milestone))
    
    //Get display coordinates of geoPosition
    var pixelPosition = map.gcsToDisplay(center);

    displayDistances.forEach( 
        distance => 
            g.append('circle')
            .attr('stroke', options.stroke)
            .attr('stroke-width', options.strokeWidth)
            .attr('fill','none')
            .attr('cx', pixelPosition.x)
            .attr('cy', pixelPosition.y)
            .attr('r', distance)
    )

    
    layer.geoOn([geo.event.zoom, geo.event.resize], function () {
        var scl = layer.renderer().scaleFactor();

        g.selectAll('text')
            .style('font-size', (20 / scl) + 'px');

        g.selectAll('circle')
            .style('stroke-width', 2 / scl);
    });
    return layer;
}

function displayDistance(map, geoPos1, geoPos2){
    var xy1 = map.gcsToDisplay(geoPos1);
    var xy2 = map.gcsToDisplay(geoPos2);

    //euclidian distance
    return Math.sqrt(Math.pow(xy1.x-xy2.x, 2) + Math.pow(xy1.y - xy2.y, 2));
}