"use strict";
require('font-awesome/css/font-awesome.min.css');
require('../css/main.css');

var proj4 = require('proj4/dist/proj4.js');
require('d3/d3.min.js');
require('hammerjs/hammer.min.js');
var geo = require('geojs/geo.min.js');

import {ppi} from './ppi';

Array.prototype.last = function() {
    return this[this.length-1];
}

window.onload = function(){
    var map = geo.map({
        node: "#map"
    });

    var osm = map.createLayer('osm', { url: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png' });
    var features = map.createLayer('feature');
    var ui = map.createLayer('ui', { zIndex: 2 });

    //Create ToolTip-Element for Plots
    var tooltip = ui.createWidget('dom', { position: { x: 0, y: 0 } });
    var tooltipElem = $(tooltip.canvas()).attr('id', 'tooltip').addClass('hidden');
    map.geoOn(geo.event.mouseclick, function (evt) {
        tooltipElem.addClass('hidden');
        trackInfoElem.toggleClass('hidden', true);
    });

    var trackInfo = ui.createWidget('dom', { position: { x: 0, y:0 } });
    var trackInfoElem = $(trackInfo.canvas()).attr('id', 'trackInfo').addClass('hidden');

    //poll controllers every 1 sec
    var controllers = gamepads(1000);

    getPosition()
    .then( (pos) => { 
        var center = {x: pos.coords.longitude, y: pos.coords.latitude};
        map.center(center).zoom(18);
        
        //mark current Position on map
        features.createFeature('point')
            .data([center])
            .position( pos => pos )
            .style( { 'fillColor':'green', 'radius':10, 'strokeColor': 'black', 'strokeWidth': 2 } )
            .draw();

        //create Distance-Rings
        //createDistanceRings(map, center, [1000, 5000, 10000])
        ppi(map, center, {stroke:'red', strokeWidth:'5px' });

        //create plot in 1km distance to each cardinal point
        proj4.defs("SENSOR:1", `+proj=aeqd +lat_0=${center.y} +lon_0=${center.x} +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs`);    
        var gpsPlots = [ [0,1000], [1000,0], [0, -1000], [-1000,0] ].map( en => proj4("SENSOR:1").inverse(en) );

        features.createFeature('point', { selectionAPI: true })
            .data(gpsPlots)
            .position( plot => { return {x: plot[0], y: plot[1] }; } )
            .style({'fillColor':'blue', 'radius':10, 'strokeColor': 'black', 'strokeWidth': 2 })
            //show details about cardinal points
            .geoOn(geo.event.feature.mouseon, function (evt) {
                var text = evt.data.join('<br>')
                if (text) {
                    tooltip.position(evt.mouse.geo);
                    tooltipElem.html(text);
                }
                tooltipElem.toggleClass('hidden', !text);})
            .draw();

        var tracks = features.createFeature('line', { selectionAPI: true })
            .data([ [], [] ])
            .style({ strokeColor: 'black', strokeWidth: 2 })
            //show details about cardinal points
            .geoOn(geo.event.feature.mouseclick, function (evt) {
                if (evt.index != undefined && evt.data != undefined) {
                    var lastKnownPosition = evt.data.last();
                    trackInfo.position(lastKnownPosition);
                    trackInfoElem.attr('data-trackId', evt.index);
                }
                if(evt.mouse.buttonsDown.right==true)
                    trackInfoElem.toggleClass('hidden', false);
            });
        
        track(0, center, tracks);
        track(1, {x: gpsPlots[0][0], y: gpsPlots[0][1] }, tracks);
        track(1, {x: gpsPlots[1][0], y: gpsPlots[1][1] }, tracks);
        track(1, {x: gpsPlots[2][0], y: gpsPlots[2][1] }, tracks);
        tracks.draw();

        setInterval( () => {
            var lastTrackSelected = trackInfoElem.attr('data-trackId');
            if(lastTrackSelected){
                var lastPosition = tracks.data()[lastTrackSelected].last()
                trackInfo.position(lastPosition);
                trackInfoElem.html(JSON.stringify(lastPosition, null, 2));
            }
        }, 1000);

        var plots = features.createFeature('point', { selectionAPI: true })
        .data([])
        .style( { 'fillColor':'red', 'radius':10, 'strokeColor': 'black', 'strokeWidth': 2 } );

        plot({x: gpsPlots[2][0], y: gpsPlots[2][1] }, plots).draw();
        
        var simTrack = center;
        setInterval( ()=>
            {
            if(controllers[0]){
                var controller = controllers[0];
                simTrack = simulateTrack(0, simTrack, tracks, controller);
            }    
            },
            1000
        )

    }).catch((error) => { map.center({x: -0.1704, y: 51.5047}).zoom(14);  console.error(error.message); });;
    
}


function getPosition(options) {
    return new Promise(function (resolve, reject) {
      navigator.geolocation.getCurrentPosition(resolve, reject, options);
    });
}

function plot(position, pointFeature){
   
    var plots = pointFeature.data();
    plots.push(position);
    pointFeature.data(plots);
    return pointFeature;
}

function track(trackId, position, lineFeature){

    var tracks = lineFeature.data();
    tracks[trackId].push(position);
    lineFeature.data(tracks);
    return lineFeature;
}

function gamepads(pollInterval){
    var controllers = [];

    window.addEventListener("gamepadconnected", (e) => controllers[e.gamepad.index] = e.gamepad );
    window.addEventListener("gamepaddisconnected", (e) => delete controllers[e.gamepad.index] );

    setInterval( ()=>scangamepads(controllers), pollInterval);
    
    return controllers;
}

function scangamepads(controllers) {
    var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads() : []);
    for (var i = 0; i < gamepads.length; i++) {
      if (gamepads[i]) {
        controllers[gamepads[i].index] = gamepads[i];
      }
    }
}

function anticipatePosition(position, xspeed, yspeed){
    var latlon = proj4(`+proj=aeqd +lat_0=${position.y} +lon_0=${position.x} +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs`). inverse([xspeed, yspeed]);
    return { x: latlon[0], y: latlon[1] };
}

function simulateTrack(trackId, position, lineFeature, gamepad){
    var xspeed = gamepad.axes[0] * 15;
    var yspeed = -gamepad.axes[1] * 15;

    var nextPos = anticipatePosition(position, xspeed, yspeed);
    track(trackId, nextPos, lineFeature);
    lineFeature.draw();
    return nextPos;
}